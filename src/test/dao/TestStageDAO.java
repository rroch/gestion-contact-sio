package test.dao;

import java.sql.Connection;
import java.sql.SQLException;
import modele.dao.Jdbc;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class TestStageDAO {
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "localhost/", "bddgestioncontact", "root", "");
        Jdbc.getInstance().connecter();
        Connection cnx = Jdbc.getInstance().getConnexion();
    }
}
