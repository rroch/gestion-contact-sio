package test.dao;

import modele.dao.EleveDAO;
import modele.dao.Jdbc;
import modele.metier.Eleve;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class TestEleveDAO {

    public static void main(String[] args) {

        java.sql.Connection cnx = null;

        try {
            test0_Connexion();
            System.out.println("Test 0 effectué : connexion\n");
            test1_SelectUnique(1);
            System.out.println("Test 1 effectué : sélection unique\n");
            test2_SelectMultiple();
            System.out.println("Test 2 effectué : sélection multiple\n");
            //test3_Insert("C999");
            System.out.println("Test 3 effectué : insertion\n");
            //test4_Update("C999");
            System.out.println("Test 4 effectué : mise à jour\n");
            //test5_Delete("C999");
            System.out.println("Test 5 effectué : suppression\n");
        } catch (ClassNotFoundException e) {
            System.err.println("Erreur de pilote JDBC : " + e);
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD
     *
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "localhost/", "bddgestioncontact", "root", "");
        Jdbc.getInstance().connecter();
        Connection cnx = Jdbc.getInstance().getConnexion();
    }

    /**
     * Affiche un client d'après son identifiant
     *
     * @param idEleve
     * @throws SQLException
     */
    public static void test1_SelectUnique(int idEleve) throws SQLException {
        Eleve cetEleve = EleveDAO.selectOne(idEleve);
        if (cetEleve != null) {
            System.out.println("Eleve d'identifiant : " + idEleve + " : " + cetEleve.toString());
        } else {
            System.out.println("L'élève d'identifiant : " + idEleve + " n'existe pas ");
        }

    }

    /**
     * Affiche tous les clients
     *
     * @throws SQLException
     */
    public static void test2_SelectMultiple() throws SQLException {
        List<Eleve> desEleves = EleveDAO.selectAll();
        System.out.println("Les élèves lus : " + desEleves.toString());
    }

    /**
     * Ajoute un client
     *
     * @param idEleve
     * @throws SQLException
     */
    /* public static void test3_Insert(int idEleve) throws SQLException {
        Eleve unEleve = new Eleve(idEleve, "Martin", "Paul");
        int nb = EleveDAO.insert(idEleve, 1);
        System.out.println("Un nouveau client a été inséré: " + nb);
        test2_SelectMultiple();
    }*/
    /**
     * Modifie un élève
     *
     * @throws SQLException
     */
    /*public static void test4_Update(int idEleve) throws SQLException {
        Eleve unEleve = new Eleve(idEleve, "Martin", "Rémi");
        int nb = EleveDAO.update(idEleve, unEleve, 2);
        System.out.println("Le client " + idEleve + " a été mis à jour: " + nb);
        test1_SelectUnique(idEleve);
    }*/
    /**
     * Supprime un enregistrement
     *
     * @throws SQLException
     */
    /*public static void test5_Delete(int idEleve) throws SQLException {
        int nb = EleveDAO.delete(idEleve);
        System.out.println("Une adresse a été supprimée: " + nb);
        test2_SelectMultiple();
    }*/
}
