package test.dao;

import modele.dao.AnneeDAO;
import modele.dao.Jdbc;
import modele.metier.Annee;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class TestAnneeDAO {
     public static void main(String[] args) {

        java.sql.Connection cnx = null;

        try {
            test0_Connexion();
            System.out.println("Test 0 effectué : connexion\n");
            test1_SelectUnique(2015);
            System.out.println("Test 1 effectué : sélection unique\n");
            test2_SelectMultiple();
            System.out.println("Test 2 effectué : sélection multiple\n");
            test3_Insert(2050);
            System.out.println("Test 3 effectué : insertion\n");
            test4_Delete(2050);
            System.out.println("Test 4 effectué : suppression\n");
        } catch (ClassNotFoundException e) {
            System.err.println("Erreur de pilote JDBC : " + e);
        } catch (SQLException e) {
            System.err.println("Erreur SQL : " + e);
        } finally {
            try {
                if (cnx != null) {
                    cnx.close();
                }
            } catch (SQLException e) {
                System.err.println("Erreur de fermeture de la connexion JDBC : " + e);
            }
        }

    }

    /**
     * Vérifie qu'une connexion peut être ouverte sur le SGBD
     *
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void test0_Connexion() throws ClassNotFoundException, SQLException {
        Jdbc.creer("jdbc:mysql://", "localhost/", "bddgestioncontact", "root", "");
        Jdbc.getInstance().connecter();
        Connection cnx = Jdbc.getInstance().getConnexion();
    }

    /**
     * Affiche une année d'après son identifiant
     *
     * @param dateAnnee
     * @throws SQLException
     */
    public static void test1_SelectUnique(int dateAnnee) throws SQLException {
        Annee cetAnnee = AnneeDAO.selectOne(dateAnnee);
        if (cetAnnee != null) {
            System.out.println("Année d'identifiant : " + dateAnnee + " : " + cetAnnee.toString());
        } else {
            System.out.println("L'année d'identifiant : " + dateAnnee + " n'existe pas ");
        }

    }

    /**
     * Affiche toutes les années
     *
     * @throws SQLException
     */
    public static void test2_SelectMultiple() throws SQLException {
        List<Annee> desAnnees = AnneeDAO.selectAll();
        System.out.println("Les années lus : " + desAnnees.toString());
    }

    /**
     * Ajoute une année
     *
     * @param dateAnnee
     * @throws SQLException
     */
    public static void test3_Insert(int dateAnnee) throws SQLException {
        Annee uneAnnee = new Annee(dateAnnee);
        int nb = AnneeDAO.insert(uneAnnee);
        System.out.println("Une nouvelle année a été inséré: " + nb);
        test2_SelectMultiple();
    }

    /**
     * Supprime un enregistrement
     *
     * @throws SQLException
     */
    public static void test4_Delete(int dateAnnee) throws SQLException {
        int nb = AnneeDAO.delete(dateAnnee);
        System.out.println("Une adresse a été supprimée: " + nb);
        test2_SelectMultiple();
    }

}
