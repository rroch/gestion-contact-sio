package test.metier;

import modele.metier.Eleve;
import modele.metier.Entreprise;
import modele.metier.Professionnel;
import modele.metier.Stage;
import modele.metier.Visite;
import java.util.ArrayList;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class TestStage {

    public static void main(String[] args) {

        // Test n°1
        System.out.println("\nTest d'instanciation et d'état");
        Stage unStage = new Stage();
        ArrayList<Stage>lesStages = null;
        Entreprise entr = new Entreprise(1, "SoftyLab", "06-40-52-36-95", "Rue Léo La Grange", 0, "Clisson",lesStages);
        Professionnel pro = new Professionnel(1, "Le Moal", "Cedric", "SLAM", "SoftyLab@gmail.com", "06-82-42-82-52", lesStages);
        ArrayList<Visite> lesVisites = new ArrayList<Visite>();
        Eleve el = new Eleve(1, "Touzard", "Valerian",lesStages);
        lesVisites.add(new Visite(1, false, true, true));
        Stage sta = new Stage(1, 2018, 7, entr, pro, el, lesVisites);
        System.out.println("Etat du stage : " + sta.toString());
    }
}
