package test.metier;

import modele.metier.Enseignant;
import modele.metier.Visite;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class TestEnseignant {

    public static void main(String[] args) {
        // Test n°1
        System.out.println("\nTest d'instanciation et d'état");
        Enseignant ens = new Enseignant(1, "Contant", "Nelly");
        Visite vi = new Visite(1, true, true, true);
        Visite vi2 = new Visite(2, true, true, true);
        ens.ajouterVisite(vi);
        ens.ajouterVisite(vi2);
        System.out.println("Etat de l'enseignant : " + ens.toString());
    }
}
