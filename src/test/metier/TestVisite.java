package test.metier;

import modele.metier.Visite;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class TestVisite {

    public static void main(String[] args) {
        // Test n°1
        System.out.println("\nTest d'instanciation et d'état");
        Visite vis = new Visite(1, false, true, true);
        System.out.println("Etat de la visite : " + vis.toString());
    }
}
