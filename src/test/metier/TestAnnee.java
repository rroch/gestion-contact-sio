package test.metier;

import modele.metier.Annee;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class TestAnnee {

    public static void main(String[] args) {
        // Test n°1
        System.out.println("\nTest d'instanciation et d'état");
        Annee ann = new Annee(2019);
        System.out.println(ann.toString());
    }
}
