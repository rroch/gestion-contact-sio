package test.metier;

import modele.metier.Annee;
import modele.metier.Eleve;
import modele.metier.Specialite;
import modele.metier.Stage;
import java.util.ArrayList;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class TestSpecialite {

    public static void main(String[] args) {
        // Test n°1
        System.out.println("\nTest d'instanciation et d'état");
        Stage unStage = new Stage();
        ArrayList<Stage>lesStages = null;
        Eleve el = new Eleve(1, "Baudry", "Clément", lesStages);
        Annee ann = new Annee(2020);
        Specialite spe = new Specialite(1, "SLAM", el, ann);

        System.out.println("Etat de la specialite : " + spe.toString());
    }
}
