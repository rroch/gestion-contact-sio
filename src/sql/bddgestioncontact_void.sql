-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  Dim 15 mars 2020 à 17:40
-- Version du serveur :  5.7.28
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bddgestioncontact`
--

-- --------------------------------------------------------

--
-- Structure de la table `annee`
--

DROP TABLE IF EXISTS `annee`;
CREATE TABLE IF NOT EXISTS `annee` (
  `dateAnnee` int(5) NOT NULL,
  PRIMARY KEY (`dateAnnee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `eleves`
--

DROP TABLE IF EXISTS `eleves`;
CREATE TABLE IF NOT EXISTS `eleves` (
  `idEleve` int(6) NOT NULL,
  `nomEleve` varchar(30) NOT NULL,
  `prenomEleve` varchar(30) NOT NULL,
  PRIMARY KEY (`idEleve`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `enseignants`
--

DROP TABLE IF EXISTS `enseignants`;
CREATE TABLE IF NOT EXISTS `enseignants` (
  `idEnsei` int(3) NOT NULL,
  `nomEnsei` varchar(30) NOT NULL,
  `prenomEsei` varchar(30) NOT NULL,
  PRIMARY KEY (`idEnsei`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `entreprises`
--

DROP TABLE IF EXISTS `entreprises`;
CREATE TABLE IF NOT EXISTS `entreprises` (
  `idEntreprise` int(10) NOT NULL,
  `nomEntreprise` varchar(30) NOT NULL,
  `numTelEntreprise` varchar(14) NOT NULL,
  `adrEntreprise` varchar(50) NOT NULL,
  `CPEntreprise` int(5) NOT NULL,
  `villeEntreprise` varchar(30) NOT NULL,
  PRIMARY KEY (`idEntreprise`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `professionnels`
--

DROP TABLE IF EXISTS `professionnels`;
CREATE TABLE IF NOT EXISTS `professionnels` (
  `idProf` int(5) NOT NULL,
  `nomProf` varchar(30) NOT NULL,
  `prenomProf` varchar(30) NOT NULL,
  `specialiteProf` varchar(5) NOT NULL,
  `mailProf` varchar(30) NOT NULL,
  `numeroTelProf` varchar(14) NOT NULL,
  `idEntreprise` int(10) NOT NULL,
  PRIMARY KEY (`idProf`),
  KEY `idEntreprise` (`idEntreprise`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `specialite`
--

DROP TABLE IF EXISTS `specialite`;
CREATE TABLE IF NOT EXISTS `specialite` (
  `idSpecialite` int(2) NOT NULL AUTO_INCREMENT,
  `libelleSpecialite` varchar(5) NOT NULL,
  `dateAnnee` int(5) NOT NULL,
  `idEleve` int(6) NOT NULL,
  PRIMARY KEY (`idSpecialite`),
  KEY `dateAnnee` (`dateAnnee`),
  KEY `idEleve` (`idEleve`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `stages`
--

DROP TABLE IF EXISTS `stages`;
CREATE TABLE IF NOT EXISTS `stages` (
  `idStage` int(20) NOT NULL,
  `anneeStage` int(5) NOT NULL,
  `periodeStage` int(3) NOT NULL,
  `idEntreprise` int(10) NOT NULL,
  `idEleve` int(6) NOT NULL,
  PRIMARY KEY (`idStage`),
  KEY `idEntreprise` (`idEntreprise`),
  KEY `idEleve` (`idEleve`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `visite`
--

DROP TABLE IF EXISTS `visite`;
CREATE TABLE IF NOT EXISTS `visite` (
  `idVisite` int(20) NOT NULL,
  `accordJury` tinyint(1) NOT NULL,
  `accord1ereStage` tinyint(1) NOT NULL,
  `accord2emeStage` tinyint(1) NOT NULL,
  `idStage` int(20) NOT NULL,
  `idEnsei` int(3) NOT NULL,
  PRIMARY KEY (`idVisite`),
  KEY `idStage` (`idStage`),
  KEY `idEnsei` (`idEnsei`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
