-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3308
-- Généré le :  Dim 15 mars 2020 à 17:36
-- Version du serveur :  5.7.28
-- Version de PHP :  7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bddgestioncontact`
--

-- --------------------------------------------------------

--
-- Structure de la table `annee`
--

DROP TABLE IF EXISTS `annee`;
CREATE TABLE IF NOT EXISTS `annee` (
  `dateAnnee` int(5) NOT NULL,
  PRIMARY KEY (`dateAnnee`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `annee`
--

INSERT INTO `annee` (`dateAnnee`) VALUES
(2012),
(2013),
(2014),
(2015),
(2016),
(2017),
(2018),
(2019),
(2020);

-- --------------------------------------------------------

--
-- Structure de la table `eleves`
--

DROP TABLE IF EXISTS `eleves`;
CREATE TABLE IF NOT EXISTS `eleves` (
  `idEleve` int(6) NOT NULL,
  `nomEleve` varchar(30) NOT NULL,
  `prenomEleve` varchar(30) NOT NULL,
  PRIMARY KEY (`idEleve`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `eleves`
--

INSERT INTO `eleves` (`idEleve`, `nomEleve`, `prenomEleve`) VALUES
(1, 'BALESTRAT', 'Rudy'),
(2, 'GIRARD ', 'Matheo'),
(3, 'LE FOURN', 'Alex'),
(4, 'RAYER', 'Alex'),
(5, 'MISSIAEN', 'Gaspar'),
(6, 'NICOD', 'Matthias'),
(7, 'PROUX', 'Titouan '),
(8, 'BERNARDEAU', 'Louis'),
(9, 'FRION', 'Arthur'),
(10, 'GIRES', 'William'),
(11, 'CHIFFOLEAU', 'Chloe'),
(12, 'PINEAU', 'Anthony');

-- --------------------------------------------------------

--
-- Structure de la table `enseignants`
--

DROP TABLE IF EXISTS `enseignants`;
CREATE TABLE IF NOT EXISTS `enseignants` (
  `idEnsei` int(3) NOT NULL,
  `nomEnsei` varchar(30) NOT NULL,
  `prenomEsei` varchar(30) NOT NULL,
  PRIMARY KEY (`idEnsei`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `enseignants`
--

INSERT INTO `enseignants` (`idEnsei`, `nomEnsei`, `prenomEsei`) VALUES
(1, 'CONTANT', 'Nelly'),
(2, 'SIMON', 'Bernard'),
(3, 'BOURGEOIS', 'Nicolas');

-- --------------------------------------------------------

--
-- Structure de la table `entreprises`
--

DROP TABLE IF EXISTS `entreprises`;
CREATE TABLE IF NOT EXISTS `entreprises` (
  `idEntreprise` int(10) NOT NULL,
  `nomEntreprise` varchar(30) NOT NULL,
  `numTelEntreprise` varchar(14) NOT NULL,
  `adrEntreprise` varchar(50) NOT NULL,
  `CPEntreprise` int(5) NOT NULL,
  `villeEntreprise` varchar(30) NOT NULL,
  PRIMARY KEY (`idEntreprise`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `entreprises`
--

INSERT INTO `entreprises` (`idEntreprise`, `nomEntreprise`, `numTelEntreprise`, `adrEntreprise`, `CPEntreprise`, `villeEntreprise`) VALUES
(1, 'Dalmace Company', '03.89.11.26.21', '15, rue Descartes', 67200, 'STRASBOURG'),
(2, 'Mercer OIUT', '03.46.28.65.33', '29, rue de Lille', 59280, 'ARMENTIÈRES'),
(3, 'Masson Company', '02.18.00.68.50', '41, rue de l\'Aigle', 97419, 'LA POSSESSION'),
(4, 'Poulin Company', '01.31.42.11.75', '46, Place de la Madeleine', 75009, 'PARIS'),
(5, 'Berthelette Company', '02.61.66.94.56', '57, rue du Château', 44800, 'SAINT-HERBLAIN'),
(6, 'Lessard Inc.', '02.40.55.99.53', '83, avenue Jules Ferry', 76300, 'SOTTEVILLE-LÈS-ROUEN'),
(7, 'Achin IT', '04.05.35.52.14', '25, Chemin des Bateliers', 73100, 'AIX-LES-BAINS'),
(8, 'InformatikUp', '08.04.06.04.05', '16, Rue du général de Gaulle', 44230, 'SAINT SEBASTIEN SUR LOIRE');

-- --------------------------------------------------------

--
-- Structure de la table `professionnels`
--

DROP TABLE IF EXISTS `professionnels`;
CREATE TABLE IF NOT EXISTS `professionnels` (
  `idProf` int(5) NOT NULL,
  `nomProf` varchar(30) NOT NULL,
  `prenomProf` varchar(30) NOT NULL,
  `specialiteProf` varchar(5) NOT NULL,
  `mailProf` varchar(30) NOT NULL,
  `numeroTelProf` varchar(14) NOT NULL,
  `idEntreprise` int(10) NOT NULL,
  PRIMARY KEY (`idProf`),
  KEY `idEntreprise` (`idEntreprise`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `professionnels`
--

INSERT INTO `professionnels` (`idProf`, `nomProf`, `prenomProf`, `specialiteProf`, `mailProf`, `numeroTelProf`, `idEntreprise`) VALUES
(1, 'LAFLEUR', 'Bernard', 'SISR', 'blafleur@gmail.com', '06.78.52.75.75', 1),
(2, 'GASTON', 'Leon', 'SLAM', 'lgaston@gmail.com', '06.78.52.02.85', 2),
(3, 'RIFTON', 'Romain', 'SISR', 'rrifton@gmail.com', '06.78.52.00.01', 2),
(4, 'PONTON', 'Clément', 'SLAM', 'cponton@gmail.com', '06.78.52.15.53', 3),
(5, 'TRASIR', 'Mael', 'SLAM', 'mtrasir@gmail.com', '06.78.52.45.31', 4),
(6, 'TRES', 'Thomas', 'SISR', 'ttres@gmail.com', '06.78.52.12.25', 5),
(7, 'PORTMAN', 'Thomas', 'SLAM', 'tportman@gmail.com', '06.78.52.82.23', 6),
(8, 'BERNARD', 'Florian', 'SISR', 'fbernard@gmail.com', '06.78.52.98.72', 7),
(9, 'SIRT', 'Kevin', 'SISR', 'ksirt@gmail.com', '06.78.52.84.85', 7),
(10, 'ARTIS', 'Arthur', 'SISR', 'aarthis@gmail.com', '06.78.52.02.58', 8);

-- --------------------------------------------------------

--
-- Structure de la table `specialite`
--

DROP TABLE IF EXISTS `specialite`;
CREATE TABLE IF NOT EXISTS `specialite` (
  `idSpecialite` int(2) NOT NULL AUTO_INCREMENT,
  `libelleSpecialite` varchar(5) NOT NULL,
  `dateAnnee` int(5) NOT NULL,
  `idEleve` int(6) NOT NULL,
  PRIMARY KEY (`idSpecialite`),
  KEY `dateAnnee` (`dateAnnee`),
  KEY `idEleve` (`idEleve`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `specialite`
--

INSERT INTO `specialite` (`idSpecialite`, `libelleSpecialite`, `dateAnnee`, `idEleve`) VALUES
(1, 'SISR', 2019, 1),
(2, 'SISR', 2019, 2),
(3, 'SISR', 2019, 3),
(4, 'SISR', 2019, 4),
(5, 'SISR', 2019, 5),
(6, 'SISR', 2019, 6),
(7, 'SLAM', 2019, 7),
(8, 'SLAM', 2019, 8),
(9, 'SLAM', 2019, 9),
(10, 'SLAM', 2019, 10),
(11, 'SLAM', 2019, 11),
(12, 'SLAM', 2019, 12),
(13, 'SLAM', 2020, 7),
(14, 'SLAM', 2020, 8),
(15, 'SLAM', 2020, 9),
(16, 'SLAM', 2020, 10),
(17, 'SLAM', 2020, 11),
(18, 'SLAM', 2020, 12),
(19, 'SISR', 2020, 1),
(20, 'SISR', 2020, 2),
(21, 'SISR', 2020, 3),
(22, 'SISR', 2020, 4),
(23, 'SISR', 2020, 5),
(24, 'SLAM', 2020, 6);

-- --------------------------------------------------------

--
-- Structure de la table `stages`
--

DROP TABLE IF EXISTS `stages`;
CREATE TABLE IF NOT EXISTS `stages` (
  `idStage` int(20) NOT NULL,
  `anneeStage` int(5) NOT NULL,
  `periodeStage` int(3) NOT NULL,
  `idEntreprise` int(10) NOT NULL,
  `idEleve` int(6) NOT NULL,
  PRIMARY KEY (`idStage`),
  KEY `idEntreprise` (`idEntreprise`),
  KEY `idEleve` (`idEleve`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `stages`
--

INSERT INTO `stages` (`idStage`, `anneeStage`, `periodeStage`, `idEntreprise`, `idEleve`) VALUES
(1, 2019, 4, 2, 5),
(2, 2019, 4, 1, 2),
(3, 2019, 4, 2, 3),
(4, 2019, 4, 3, 4),
(5, 2019, 4, 4, 5),
(6, 2019, 4, 5, 6),
(7, 2019, 4, 6, 7),
(8, 2019, 4, 7, 8),
(9, 2019, 4, 8, 9),
(10, 2019, 4, 1, 10),
(11, 2020, 7, 2, 11),
(12, 2020, 7, 3, 12),
(13, 2020, 7, 4, 1),
(14, 2020, 7, 5, 2),
(15, 2020, 7, 6, 3),
(16, 2020, 7, 7, 4),
(17, 2020, 7, 8, 5),
(18, 2020, 7, 1, 6),
(19, 2020, 7, 2, 7),
(20, 2020, 7, 3, 8);

-- --------------------------------------------------------

--
-- Structure de la table `visite`
--

DROP TABLE IF EXISTS `visite`;
CREATE TABLE IF NOT EXISTS `visite` (
  `idVisite` int(20) NOT NULL,
  `accordJury` tinyint(1) NOT NULL,
  `accord1ereStage` tinyint(1) NOT NULL,
  `accord2emeStage` tinyint(1) NOT NULL,
  `idStage` int(20) NOT NULL,
  `idEnsei` int(3) NOT NULL,
  PRIMARY KEY (`idVisite`),
  KEY `idStage` (`idStage`),
  KEY `idEnsei` (`idEnsei`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `visite`
--

INSERT INTO `visite` (`idVisite`, `accordJury`, `accord1ereStage`, `accord2emeStage`, `idStage`, `idEnsei`) VALUES
(1, 0, 1, 0, 1, 2),
(2, 1, 0, 0, 2, 3),
(3, 1, 1, 1, 3, 2),
(4, 0, 1, 1, 4, 1),
(5, 0, 0, 0, 5, 2),
(6, 1, 1, 1, 6, 3),
(7, 0, 1, 1, 7, 3),
(8, 1, 0, 1, 8, 2),
(9, 1, 0, 0, 1, 2),
(10, 0, 0, 0, 2, 1),
(11, 1, 1, 1, 3, 1),
(12, 0, 1, 1, 4, 1),
(13, 1, 0, 1, 5, 1),
(14, 1, 0, 1, 6, 2),
(15, 1, 0, 0, 7, 1),
(16, 1, 0, 1, 8, 3),
(17, 1, 1, 1, 1, 3),
(18, 0, 0, 0, 2, 2),
(19, 1, 0, 1, 3, 1),
(20, 1, 1, 1, 4, 1);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
