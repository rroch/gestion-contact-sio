package Modele.dao;

import modele.metier.Visite;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class VisiteDAO {
    
      
    /**
     * lire tous les enregistrements de la table Visite
     *
     * @return une collection d'instances de la classe Visite
     * @throws SQLException
     */
    public static List<Visite> selectAll() throws SQLException {
        List<Visite> lesVisites = new ArrayList<Visite>();
        Visite uneVisite = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM VISITE";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("ID");
            boolean accordJury = rs.getBoolean("ACCORDJURY");
            boolean accordPremStage = rs.getBoolean("ACCORDPREMSTAGE");
            boolean accordSecStage = rs.getBoolean("ACCORDSECSTAGE");
            uneVisite = new Visite(id, accordJury, accordPremStage, accordSecStage);
            lesVisites.add(uneVisite);
        }
        return lesVisites;
    }
    
    public static Visite selectOne(int idVisite) throws SQLException {
        Visite uneVisite = null;
        ResultSet rs = null;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM VISITE WHERE idVisite= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idVisite);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("ID");
            boolean accordJury = rs.getBoolean("ACCORDJURY");
            boolean accordPremStage = rs.getBoolean("ACCORDPREMSTAGE");
            boolean accordSecStage = rs.getBoolean("ACCORDSECSTAGE");
            uneVisite = new Visite(id, accordJury, accordPremStage, accordSecStage);
        }
        return uneVisite;
    }    
    
     /**
     * insert : ajouter un enregistrement dans la table Visite
     *
     * @param uneVisite : instance de la classe Client à enregistrer dans la
     * table VISITE
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException
     */
    public static int insert(Visite uneVisite, boolean accordJurry, boolean  accord1ereStage, boolean accord2emeStage) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        PreparedStatement pstmt;
        requete = "INSERT INTO VISITE (IDVISITE, ACCORDJURRY, ACCORDPREMSTAGE, ACCORDSECSTAGE)";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, uneVisite.getId());
        pstmt.setBoolean(2, accordJurry);
        pstmt.setBoolean(3, accord1ereStage);
        pstmt.setBoolean(4, accord2emeStage);

        nb = pstmt.executeUpdate();
        return nb;
    }
    
    /**
     * update : modifier un enregistrement de la table VISITE
     *
     * @param id : identifiant conceptuel de la Visite à modifier
     * @param uneVisite : instance de la classe Visite contenant les nouvelles
     * valeurs à enregistrer dans la table VISITE sous le même identifiant
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException
     */
    public static int update(Visite uneVisite, boolean accordJurry, boolean  accord1ereStage, boolean accord2emeStage) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        PreparedStatement pstmt;
        requete = "UPDATE ENTREPRISE SET IDVISITE = ?,ACCORDJURRY = ?,ACCORDPREMSTAGE = ?, ACCORDSECSTAGE = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, uneVisite.getId());
        pstmt.setBoolean(2, accordJurry);
        pstmt.setBoolean(3, accord1ereStage);
        pstmt.setBoolean(4, accord2emeStage);
        
        nb = pstmt.executeUpdate();
        return nb;
    }
    
    /**
     * delete : supprimer un enregistrement de la table VISITE
     * @param idVisite : identifiant conceptuel de la visite à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception est émise
     * @throws SQLException 
     */
    public static int delete(int idVisite) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        PreparedStatement pstmt;
        requete = "DELETE FROM VISITE WHERE IDVISITE = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idVisite);
        nb = pstmt.executeUpdate();
        return nb;
    }
}
