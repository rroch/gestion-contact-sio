/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package exporterCSV;

import java.io.BufferedReader;
import java.io.FileReader;

/**
 *
 * @author RochR
 */
public class Helper {

    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new FileReader("src/exporterCSV/bddgestioncontact.csv"));
        String ligne = null;
        while ((ligne = br.readLine()) != null) {
            // Retourner la ligne dans un tableau
            String[] data = ligne.split(",");

            // Afficher le contenu du tableau
            for (String val : data) {
                System.out.println(val);
            }
        }
        br.close();
    }
}
