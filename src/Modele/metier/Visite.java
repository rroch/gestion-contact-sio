package Modele.metier;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class Visite {

    private int id;
    private boolean accordJury = false;
    private boolean accordPremStage = false;
    private boolean accordSecStage = false;

    /**
     * Constructeur Visite (avec paramètres)
     *
     * @param unId Identifiant Visite
     * @param accordJury Accord pour participer au jury
     * @param accordPremStage Accord pour prendre des stagiaires en première
     * année
     * @param accordSecStage Accord pour prendre des stagiaires en deuxième
     * année
     */
    public Visite(int unId, boolean accordJury, boolean accordPremStage, boolean accordSecStage) {
        this.id = unId;
        this.accordJury = accordJury;
        this.accordPremStage = accordPremStage;
        this.accordSecStage = accordSecStage;
    }

    /**
     * Constructeur Visite (sans paramètre)
     */
    public Visite() {

    }
    //toString
    @Override
    public String toString() {
        return "Id Visite: " + this.id + ", accordJury: " + this.accordJury + ", accordPremStage: " + this.accordPremStage + ", accordSecStage: " + this.accordSecStage;
    }
    //Gettter and Setter

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public boolean isAccordJury() {
        return accordJury;
    }

    public void setAccordJury(boolean accordJury) {
        this.accordJury = accordJury;
    }

    public boolean isAccordPremStage() {
        return accordPremStage;
    }

    public void setAccordPremStage(boolean accordPremStage) {
        this.accordPremStage = accordPremStage;
    }

    public boolean isAccordSecStage() {
        return accordSecStage;
    }

    public void setAccordSecStage(boolean accordSecStage) {
        this.accordSecStage = accordSecStage;
    }

}
