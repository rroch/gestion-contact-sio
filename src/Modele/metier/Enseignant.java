package Modele.metier;

import java.util.ArrayList;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class Enseignant {

    private int id;
    private String nom;
    private String prenom;
    private ArrayList<Visite> lesVisites;

    /**
     * Constructeur Enseignant (avec paramètres)
     *
     * @param unId Identifiant de l'enseignant
     * @param unNom Nom de l'enseignant
     * @param unPrenom Prenom de l'enseignant
     */
    public Enseignant(int unId, String unNom, String unPrenom) {
        this.id = unId;
        this.nom = unNom;
        this.prenom = unPrenom;
        this.lesVisites = new ArrayList<Visite>();
    }

    /**
     * Constructeur Enseignant (sans paramètre)
     */
    public Enseignant() {
    }

    //toString
    @Override
    public String toString() {
        return "Id Enseignant: " + this.id + ", Nom: " + this.nom + ", Prenom: " + this.prenom + ", Visite:" + this.lesVisites;
    }

    //Getter and Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public ArrayList<Visite> getLesVisites() {
        return lesVisites;
    }

    public void setLesVisites(ArrayList<Visite> lesVisites) {
        this.lesVisites = lesVisites;
    }

    public void ajouterVisite(Visite vi) {
        lesVisites.add(vi);
    }
}
