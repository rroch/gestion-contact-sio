package Modele.metier;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class Annee {

    private int dateAnnee;

    /**
     * Constructeur Année (avec paramètres)
     *
     * @param uneAnnee Année
     */
    public Annee(int uneAnnee) {
        this.dateAnnee = uneAnnee;
    }

    /**
     * Constructeur Annéee (sans paramètre)
     */
    public Annee() {
    }

    //toString
    @Override
    public String toString() {
        return "dateAnnee: " + this.dateAnnee;
    }

    //Getter and Setter
    public int getDateAnnee() {
        return this.dateAnnee;
    }

    public void setDateAnnee(int dateAnnee) {
        this.dateAnnee = dateAnnee;
    }

}
