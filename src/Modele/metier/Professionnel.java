package Modele.metier;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class Professionnel {

    private int id;
    private String nom;
    private String prenom;
    private String spe;
    private String mail;
    private String tel;

    /**
     * Constructeur Professionnel (avec paramètres)
     *
     * @param unId Identifiant du professionnel
     * @param unNom Nom du professionnel
     * @param unPrenom Prenom du professionnel
     * @param uneSpe Specialite du professionnel (SISR ou SLAM)
     * @param unMail Email du professionnel
     * @param unTel Téléphone du professionnel
     */
    public Professionnel(int unId, String unNom, String unPrenom, String uneSpe, String unMail, String unTel) {
        this.id = unId;
        this.nom = unNom;
        this.prenom = unPrenom;
        this.spe = uneSpe;
        this.mail = unMail;
        this.tel = unTel;
    }

    /**
     * Constructeur Professionnel (sans paramètre)
     */
    public Professionnel() {
    }

    //toString
    @Override
    public String toString() {
        return "Id Professionnel: " + this.id + ", Nom: " + this.nom + ", Prenom: " + this.prenom + ", Spécialité: " + this.spe + ", Email: " + this.mail + ", Téléphone: " + this.tel;
    }

    //Getter and Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getSpe() {
        return spe;
    }

    public void setSpe(String spe) {
        this.spe = spe;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

}
