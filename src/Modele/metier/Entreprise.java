package Modele.metier;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class Entreprise {

    private int id;
    private String nom;
    private String tel;
    private String adr;
    private int cp;
    private String ville;

    /**
     * Constructeur Entreprise (avec paramètres)
     *
     * @param unId Identifiant de l'entreprise
     * @param unNom Nom de l'entreprise
     * @param unTel Téléphone de l'entreprise
     * @param uneAdr Adresse de l'entreprise
     * @param unCp Code Postal de l'entreprise
     * @param uneVille Ville de l'entreprise
     */
    public Entreprise(int unId, String unNom, String unTel, String uneAdr, int unCp, String uneVille) {
        this.id = unId;
        this.nom = unNom;
        this.tel = unTel;
        this.adr = uneAdr;
        this.cp = unCp;
        this.ville = uneVille;

    }

    /**
     * Constructeur Entreprise (sans paramètre)
     */
    public Entreprise() {

    }

    //toString
    @Override
    public String toString() {
        return "Id Entreprise: " + this.id + ", Nom: " + this.nom + ", Tel: " + this.tel + ", Adresse: " + this.adr + ", CP: " + this.cp + ", NomVille: " + this.ville;
    }

    //Getter and Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getAdr() {
        return adr;
    }

    public void setAdr(String adr) {
        this.adr = adr;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

}
