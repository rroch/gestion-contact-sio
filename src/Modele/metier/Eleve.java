package Modele.metier;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class Eleve {

    private int id;
    private String nom;
    private String prenom;

    /**
     * Constructeur Eleve (avec paramètres)
     *
     * @param unId Identifiant de l'élève
     * @param unNom Nom de l'élève
     * @param unPrenom Prénom de l'élève
     */
    public Eleve(int unId, String unNom, String unPrenom) {
        this.id = unId;
        this.nom = unNom;
        this.prenom = unPrenom;
    }

    /**
     * Constructeur Eleve (sans paramètre)
     */
    public Eleve() {

    }

    //toString
    @Override
    public String toString() {
        return "Id Eleve: " + this.id + ", Nom: " + this.nom + ", Prenom: " + this.prenom;
    }

    //Getter and Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

}
