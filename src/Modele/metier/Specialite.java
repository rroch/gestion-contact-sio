package Modele.metier;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */

public class Specialite {
    
    private int id;
    private String libelle;
    private Eleve eleve;
    private Annee annee;

    /**
     * Constructeur Specialite (avec paramètres)
     * @param unId
     * @param unLibelle
     * @param unEleve
     * @param uneAnnee 
     */
    public Specialite(int unId, String unLibelle, Eleve unEleve, Annee uneAnnee) {
        this.id = unId;
        this.libelle = unLibelle;
        this.eleve = unEleve;
        this.annee = uneAnnee;
    }

    /**
     * Constructeur Specialite (sans paramètre)
     */
    public Specialite() {
    }

    //toString
    @Override
    public String toString() {
        return "Id Spécialité: " + this.id + ", Libelle: " + this.libelle+", Libelle: "+this.eleve+", Année: "+this.annee;
    }

    //Getter and Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public Eleve getEleve() {
        return eleve;
    }

    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    public Annee getAnnee() {
        return annee;
    }

    public void setAnnee(Annee annee) {
        this.annee = annee;
    }
}
