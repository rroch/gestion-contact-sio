package Modele.metier;

import java.util.ArrayList;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class Stage {

    private int id;
    private int anneeStage;
    private int periodeStage;
    private Entreprise entreprise;
    private Professionnel professionnel;
    private Eleve eleve;
    private ArrayList<Visite> lesVisites;

    /**
     * Constructeur Stage (avec paramètres)
     * @param unId
     * @param uneAnnee
     * @param unePeriode
     * @param uneEntreprise
     * @param unProfessionnel
     * @param unEleve
     * @param lesVisites 
     */
    public Stage(int unId, int uneAnnee, int unePeriode, Entreprise uneEntreprise, Professionnel unProfessionnel, Eleve unEleve, ArrayList<Visite> lesVisites) {
        this.id = unId;
        this.anneeStage = uneAnnee;
        this.periodeStage = unePeriode;
        this.entreprise = uneEntreprise;
        this.professionnel = unProfessionnel;
        this.eleve = unEleve;
        this.lesVisites = lesVisites;
    }
    /**
     * Constructeur Stage (sans paramètre)
     */
    public Stage() {
    }

    @Override
    public String toString() {
        return "Id Stage: " + this.id + ", Année de stage: " + this.anneeStage + ", Période du stage: " + this.periodeStage + ", Entreprise: " + this.entreprise + ", Profesionnel: " + this.professionnel + ", Eleve: " + this.eleve + ", Les Visites: " + this.lesVisites;
    }

    //Getter and Setter
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getAnneeStage() {
        return anneeStage;
    }

    public void setAnneeStage(int anneeStage) {
        this.anneeStage = anneeStage;
    }

    public int getPeriodeStage() {
        return periodeStage;
    }

    public void setPeriodeStage(int periodeStage) {
        this.periodeStage = periodeStage;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    public Professionnel getProfessionnel() {
        return professionnel;
    }

    public void setProfessionnel(Professionnel professionnel) {
        this.professionnel = professionnel;
    }

    public Eleve getEleve() {
        return eleve;
    }

    public void setEleve(Eleve eleve) {
        this.eleve = eleve;
    }

    public ArrayList<Visite> getLesVisites() {
        return lesVisites;
    }

    public void setLesVisites(ArrayList<Visite> lesVisites) {
        this.lesVisites = lesVisites;
    }

    public void ajouterVisite(Visite vi) {
        int i = 0;
        while (lesVisites != null) {
            i++;
        }
        lesVisites.set(i,vi);
    }
}
