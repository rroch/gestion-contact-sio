package modele.dao;

import Modele.metier.Visite;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class VisiteDAO {
     public static Visite selectOne(int idVisite) throws SQLException {
        Visite uneVisite = null;
        ResultSet rs = null;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM VISITE WHERE idVisite= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idVisite);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            uneVisite = VisiteDAO.visiteFromResultSet(rs);
        }
        return uneVisite;
    }
      
       /**
     * lire tous les enregistrements de la table Visite
     *
     * @return une collection d'instances de la classe Visite
     * @throws SQLException
     */
    public static List<Visite> selectAll() throws SQLException {
        List<Visite> lesVisites = new ArrayList<Visite>();
        Visite uneVisite = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM VISITE";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            uneVisite = VisiteDAO.visiteFromResultSet(rs);
            lesVisites.add(uneVisite);
        }
        return lesVisites;
    }
    
    /**
     * Extrait un enregistrement du "ResultSet" issu de la table Visite
     *
     * @param rs : ResultSet lu dans la table Visite
     * @return instance de Entreprise, initialisée d'après le premier enregistrement
     * du ResultSet
     * @throws SQLException
     */
    private static Visite visiteFromResultSet(ResultSet rs) throws SQLException {
        Visite uneVisite = null;
        int idVisite= rs.getInt("idVisite");
        boolean accordJurry = rs.getBoolean("accordJurry");
        boolean accord1ereStage = rs.getBoolean("accord1ereStage");
        boolean accord2emeStage = rs.getBoolean("accord2emeStage");
        return uneVisite;
    }
    
     /**
     * conversion de java.util.Date vers java.sql.Date
     *
     * @param uneDate java.util.Date
     * @return java.sql.Date
     */
    public static java.sql.Date toSqlDate(java.util.Date uneDate) {
        return new java.sql.Date(uneDate.getTime());
    }

    /**
     * conversion de java.sql.Date vers java.util.Date
     *
     * @param uneDate java.sql.Date
     * @return java.util.Date
     */
    public static java.util.Date toUtilDate(java.sql.Date uneDate) {
        return new java.util.Date(uneDate.getTime());
    }
    
    
     /**
     * insert : ajouter un enregistrement dans la table Visite
     *
     * @param uneVisite : instance de la classe Client à enregistrer dans la
     * table VISITE
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException
     */
    public static int insert(Visite uneVisite, boolean accordJurry, boolean  accord1ereStage, boolean accord2emeStage) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "INSERT INTO VISITE (IDVISITE, ACCORDJURRY, ACCORD1ERSTAGE, ACCORD2EMESTAGE)";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, uneVisite.getId());
        pstmt.setBoolean(2, accordJurry);
        pstmt.setBoolean(3, accord1ereStage);
        pstmt.setBoolean(4, accord2emeStage);

        nb = pstmt.executeUpdate();
        return nb;
    }
    
    /**
     * update : modifier un enregistrement de la table CLIENT
     *
     * @param idEleve : identifiant conceptuel du client à modifier
     * @param unEleve : instance de la classe Client contenant les nouvelles
     * valeurs à enregistrer dans la table CLIENT sous le même identifiant
     * @param idAdresse : identifiant relationnel de la table Adresse (clef étrangère)
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException
     */
    public static int update(Visite uneVisite, boolean accordJurry, boolean  accord1ereStage, boolean accord2emeStage) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "UPDATE ENTREPRISE SET IDVISITE = ?,ACCORDJURRY = ?,ACCORD1ERSTAGE = ?, ACCORD2EMESTAGE = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, uneVisite.getId());
        pstmt.setBoolean(2, accordJurry);
        pstmt.setBoolean(3, accord1ereStage);
        pstmt.setBoolean(4, accord2emeStage);
        
        nb = pstmt.executeUpdate();
        return nb;
    }
    
    /**
     * delete : supprimer un enregistrement de la table ENTREPRISE
     * @param idEntrepise : identifiant conceptuel du client à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception est émise
     * @throws SQLException 
     */
    public static int delete(int idVisite) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "DELETE FROM VISITE WHERE IDVISITE = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idVisite);
        nb = pstmt.executeUpdate();
        return nb;
    }
}
