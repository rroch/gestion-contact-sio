/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import Modele.metier.Professionnel;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author RochR
 */
public class ProfessionnelDAO {
    /**
     * Extraction d'un professionnel, sur son identifiant
     * @param idProfessionnel identifiant
     * @return objet Professionnel
     * @throws SQLException 
     */
    public static Professionnel selectOne(int idProfessionnel) throws SQLException {
        Professionnel unProfessionnel = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM PROFESSIONNELS WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idProfessionnel);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("IDPROF");
            String nom = rs.getString("NOMPROF");
            String prenom = rs.getString("PRENOMPROF");
            String spe = rs.getString("SPECIALITEPROF");
            String mail = rs.getString("MAILPROF");
            String tel = rs.getString("NUMEROTELPROF");
            unProfessionnel = new Professionnel(id, nom, prenom, spe, mail, tel);
        }
        return unProfessionnel;
    }
     

    /**
     * Extraction de tous les professionnels
     * @return collection de professionnels
     * @throws SQLException 
     */
    public static List<Professionnel> selectAll() throws SQLException {
        List<Professionnel> lesProfessionnels = new ArrayList<Professionnel>();
        Professionnel unProfessionnel;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM PROFESSIONNElS";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("IDPROF");
            String nom = rs.getString("NOMPROF");
            String prenom = rs.getString("PRENOMPROF");
            String spe = rs.getString("SPECIALITEPROF");
            String mail = rs.getString("MAILPROF");
            String tel = rs.getString("NUMEROTELPROF");
            unProfessionnel = new Professionnel(id, nom, prenom, spe, mail, tel);
            lesProfessionnels.add(unProfessionnel);
        }
        return lesProfessionnels;
    }

    public static int insert(Professionnel unProfessionnel) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "INSERT INTO PROFESSIONNELS (IDPROF, NOMPROF, PRENOMPROF, SPECIALITEPROF, MAILPROF, NUMEROTELPROF) VALUES (?, ?, ?, ?, ?, ?)";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, unProfessionnel.getId());
        pstmt.setString(2, unProfessionnel.getNom());
        pstmt.setString(3, unProfessionnel.getPrenom());
        pstmt.setString(4, unProfessionnel.getSpe());
        pstmt.setString(5, unProfessionnel.getMail());
        pstmt.setString(6, unProfessionnel.getTel());
        nb = pstmt.executeUpdate();
        return nb;
    }

    public static int update(int idProfessionnel, Professionnel unProfessionnel) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "UPDATE PROFESSIONNELS SET NOMPROF = ? , PRENOMPROF = ?, SPECIALITEPROF = ?, MAILPROF = ?, NUMEROTELPROF = ? WHERE IDPROF = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setString(1, unProfessionnel.getNom());
        pstmt.setString(2, unProfessionnel.getPrenom());
        pstmt.setString(3, unProfessionnel.getSpe());
        pstmt.setString(4, unProfessionnel.getMail());
        pstmt.setString(5, unProfessionnel.getTel());
        pstmt.setInt(6, idProfessionnel);
        nb = pstmt.executeUpdate();
        return nb;
    }

    public static int delete(int idProfessionnel) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "DELETE FROM PROFESSIONNELS WHERE IDPROF = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idProfessionnel);
        nb = pstmt.executeUpdate();
        return nb;
    }
}
