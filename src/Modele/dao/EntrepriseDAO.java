package modele.dao;

import Modele.metier.Entreprise;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class EntrepriseDAO {
    public static Entreprise selectOne(int idEntreprise) throws SQLException {
        Entreprise uneEntreprise = null;
        ResultSet rs = null;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ELEVE WHERE idEleve= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idEntreprise);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("ID");
            String nom = rs.getString("NOM");
            String tel = rs.getString("TEL");
            String adr = rs.getString("ADR");
            int cp = rs.getInt("CP");
            String ville = rs.getString("VILLE");
            uneEntreprise = new Entreprise(id, nom, tel, adr, cp, ville);
        }
        return uneEntreprise;
    }
      
       /**
     * lire tous les enregistrements de la table ENTREPRISE
     *
     * @return une collection d'instances de la classe Entreprise
     * @throws SQLException
     */
    public static List<Entreprise> selectAll() throws SQLException {
        List<Entreprise> lesEntreprises = new ArrayList<Entreprise>();
        Entreprise uneEntreprise = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ENTREPRISE";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("ID");
            String nom = rs.getString("NOM");
            String tel = rs.getString("TEL");
            String adr = rs.getString("ADR");
            int cp = rs.getInt("CP");
            String ville = rs.getString("VILLE");
            uneEntreprise = new Entreprise(id, nom, tel, adr, cp, ville);
            lesEntreprises.add(uneEntreprise);
        }
        return lesEntreprises;
    }
    
     /**
     * insert : ajouter un enregistrement dans la table Entreprise
     *
     * @param uneEntreprise : instance de la classe Entreprise à enregistrer dans la
     * table ENTREPRISE
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException
     */
    public static int insert(Entreprise uneEntreprise, String unNom, String numero, String uneAdr, int Cdp, String ville) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "INSERT INTO ENTREPRISE (IDENTREPRISE, NOMEENTREPRISE, NUMENTREPRISE, ADRENTREPRISE, CPENTREPRISE, VILLEENTREPRISE)";
        requete += " VALUES (?, ?, ?, ?, ?, ?)";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, uneEntreprise.getId());
        pstmt.setString(2, unNom);
        pstmt.setString(3, numero);
        pstmt.setString(4, uneAdr);
        pstmt.setInt(5, Cdp);
        pstmt.setString(6, ville);

        nb = pstmt.executeUpdate();
        return nb;
    }
    
    /**
     * update : modifier un enregistrement de la table ENTREPRISE
     *
     * @param idEntreprise : identifiant conceptuel de l'Entreprise à modifier
     * @param uneEntreprise : instance de la classe Entreprise contenant les nouvelles
     * valeurs à enregistrer dans la table ENTREPRISE sous le même identifiant
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException
     */
    public static int update(int idEntreprise, Entreprise uneEntreprise, String unNom, String numero, String uneAdr, int Cdp, String ville) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "UPDATE ENTREPRISE SET NOMENTREPRISE = ?,NUMENTREPRISE = ?, ADRENTREPRISE = ?, CPENTREPRISE = ?, VILLEENTREPRISE = ?";
        requete += " WHERE IDENTREPRISE = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setString(1, unNom);
        pstmt.setString(2, numero);
        pstmt.setString(2, uneAdr);
        pstmt.setInt(4, Cdp);
        pstmt.setString(5, ville);
        pstmt.setInt(6, idEntreprise);
        
        nb = pstmt.executeUpdate();
        return nb;
    }
    
    /**
     * delete : supprimer un enregistrement de la table ENTREPRISE
     * @param idEntreprise : identifiant conceptuel de l'Entreprise à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception est émise
     * @throws SQLException 
     */
    public static int delete(int idEntreprise) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "DELETE FROM ENTREPRISE WHERE IDENTREPRISE = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idEntreprise);
        nb = pstmt.executeUpdate();
        return nb;
    }
}
