package modele.dao;

import Modele.metier.Annee;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class AnneeDAO {
    /**
     * selectOne : lire un enregistrement dans la table ANNEE
     * @param idDateAnnee : identifiant conceptuel de l'Annee recherché
     * @return une instance de la classe Adresse
     * @throws SQLException
     */
    public static Annee selectOne(int idDateAnnee) throws SQLException {
        Annee uneAnnee = null;
        ResultSet rs = null;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ANNEE WHERE DATEANNEE= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idDateAnnee);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int dateAnnee = rs.getInt("DATEANNEE");
            uneAnnee = new Annee(dateAnnee);
        }
        return uneAnnee;
    }

    /**
     * lire tous les enregistrements de la table ANNEE
     *
     * @return une collection d'instances de la classe Annee
     * @throws SQLException
     */
    public static List<Annee> selectAll() throws SQLException {
        List<Annee> lesAnnees = new ArrayList<Annee>();
        Annee uneAnnee = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ANNEE";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int dateAnnee = rs.getInt("DATEANNEE");
            uneAnnee = new Annee(dateAnnee);
            lesAnnees.add(uneAnnee);
        }
        return lesAnnees;
    }

    /**
     * insert : ajouter un enregistrement dans la table ANNEE
     *
     * @param uneAnnee : instance de la classe Annee à enregistrer dans la
     * table ANNEE
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException
     */
    public static int insert(Annee uneAnnee) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "INSERT INTO ANNEE (DATEANNEE) VALUES (?)";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, uneAnnee.getDateAnnee());
        nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * delete : supprimer un enregistrement de la table ANNEE
     * @param dateAnnee : identifiant conceptuel de l'annee à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception est émise
     * @throws SQLException 
     */
    public static int delete(int dateAnnee) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "DELETE FROM ANNEE WHERE DATEANNEE = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, dateAnnee);
        nb = pstmt.executeUpdate();
        return nb;
    }
}
