/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modele.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import Modele.metier.Enseignant;

/**
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 */
public class EnseignantDAO {
    /**
     * Extraction d'un enseignant, sur son identifiant
     * @param idEnseignant identifiant
     * @return objet Enseignant
     * @throws SQLException 
     */
    public static Enseignant selectOne(int idEnseignant) throws SQLException {
        Enseignant unEnseignant = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ENSEIGNANT WHERE ID= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idEnseignant);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("ID");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            unEnseignant = new Enseignant(id, nom, prenom);
        }
        return unEnseignant;
    }
     

    /**
     * Extraction de tous les enseignants
     * @return collection d'enseignants
     * @throws SQLException 
     */
    public static List<Enseignant> selectAll() throws SQLException {
        List<Enseignant> lesEnseignants = new ArrayList<Enseignant>();
        Enseignant unEnseignant;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ENSEIGNANT";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("ID");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            unEnseignant = new Enseignant(id, nom, prenom);
            lesEnseignants.add(unEnseignant);
        }
        return lesEnseignants;
    }

    public static int insert(Enseignant unEnseignant) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "INSERT INTO ENSEIGNANT (ID, NOM, PRENOM) VALUES (?, ?, ?)";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, unEnseignant.getId());
        pstmt.setString(2, unEnseignant.getNom());
        pstmt.setString(3, unEnseignant.getPrenom());
        nb = pstmt.executeUpdate();
        return nb;
    }

    public static int update(int idEnseignant, Enseignant unEnseignant) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "UPDATE ENSEIGNANT SET NOM = ? , PRENOM = ? WHERE ID = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setString(1, unEnseignant.getNom());
        pstmt.setString(2, unEnseignant.getPrenom());
        pstmt.setInt(3, idEnseignant);
        nb = pstmt.executeUpdate();
        return nb;
    }

    public static int delete(int idEnseignant) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "DELETE FROM ENSEIGNANT WHERE ID = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idEnseignant);
        nb = pstmt.executeUpdate();
        return nb;
    }
}
