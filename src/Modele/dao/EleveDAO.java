package modele.dao;

import Modele.metier.Eleve;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Description of LieuDAO Classe métier : Eleve
 *
 * @author vtouzard, llemauff, cbaudry, rroch
 * @version 2020
 */
public class EleveDAO {

    public static Eleve selectOne(int idEleve) throws SQLException {
        Eleve unEleve = null;
        ResultSet rs = null;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ELEVES WHERE idEleve= ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idEleve);
        rs = pstmt.executeQuery();
        if (rs.next()) {
            int id = rs.getInt("ID");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            unEleve = new Eleve(id, nom, prenom);
        }
        return unEleve;
    }

    /**
     * lire tous les enregistrements de la table ELEVE
     *
     * @return une collection d'instances de la classe Eleve
     * @throws SQLException
     */
    public static List<Eleve> selectAll() throws SQLException {
        List<Eleve> lesEleves = new ArrayList<Eleve>();
        Eleve unEleve = null;
        ResultSet rs;
        PreparedStatement pstmt;
        Jdbc jdbc = Jdbc.getInstance();
        // préparer la requête
        String requete = "SELECT * FROM ELEVE";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        rs = pstmt.executeQuery();
        while (rs.next()) {
            int id = rs.getInt("ID");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            unEleve = new Eleve(id, nom, prenom);
            lesEleves.add(unEleve);
        }
        return lesEleves;
    }

    /**
     * insert : ajouter un enregistrement dans la table CLIENT
     *
     * @param unEleve : instance de la classe Client à enregistrer dans la table
     * CLIENT
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException
     */
    public static int insert(Eleve unEleve, String unNom, String unPrenom) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "INSERT INTO ELEVES (IDELEVE, NOMELEVE, PRENOMELEVE)";
        requete += " VALUES (null, ?, ?)";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, unEleve.getId());
        pstmt.setString(2, unNom);
        pstmt.setString(3, unPrenom);
        nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * update : modifier un enregistrement de la table CLIENT
     *
     * @param idEleve : identifiant conceptuel du client à modifier
     * @param unEleve : instance de la classe Client contenant les nouvelles
     * valeurs à enregistrer dans la table CLIENT sous le même identifiant
     * @return : 1 si l'enregistrement a eu lieu ; en cas d'erreur, une
     * exception est émise
     * @throws SQLException
     */
    public static int update(Eleve unEleve, String unNom, String unPrenom) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "UPDATE ELEVE SET IDELEVE = ?,NOM = ?,PRENOM = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, unEleve.getId());
        pstmt.setString(2, unNom);
        pstmt.setString(3, unPrenom);
        nb = pstmt.executeUpdate();
        return nb;
    }

    /**
     * delete : supprimer un enregistrement de la table ELEVE
     *
     * @param idEleve : identifiant conceptuel du client à supprimer
     * @return : 1 si la suppression a eu lieu ; en cas d'erreur, une exception
     * est émise
     * @throws SQLException
     */
    public static int delete(int idEleve) throws SQLException {
        int nb;
        Jdbc jdbc = Jdbc.getInstance();
        String requete;
        ResultSet rs;
        PreparedStatement pstmt;
        requete = "DELETE FROM ELEVE WHERE IDELEVE = ?";
        pstmt = jdbc.getConnexion().prepareStatement(requete);
        pstmt.setInt(1, idEleve);
        nb = pstmt.executeUpdate();
        return nb;
    }
}
